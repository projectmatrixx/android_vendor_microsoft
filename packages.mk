# Inherit from phone link config
TARGET_PHONE_LINK_SUPPORTED ?= false
ifeq ($(TARGET_PHONE_LINK_SUPPORTED),true)
PRODUCT_PACKAGES += \
    CrossDeviceServiceBroker \
    DeviceIntegrationService \
    LinkToWindows \
    virtual_keyboard

# Device Integration
PRODUCT_SYSTEM_PROPERTIES += \
    persist.sys.device_integration.disable?=false
endif
